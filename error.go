package e

import (
	"encoding/json"
)

type Error struct {
	Msg string `json:"error"`
}

func NewError(msg string) Error {
	return Error{Msg: msg}
}

func NewErrorFrom(err error) Error {
	return Error{Msg: err.Error()}
}

func (e Error) Error() string {
	return e.Msg
}

func Parse(data []byte) (err Error) {
	a := json.Unmarshal(data, &err)
	if a != nil {
		return Error{Msg: string(data)}
	}
	return
}
